package ru.forwolk.auth.test;

import org.junit.Before;
import org.junit.Test;
import ru.forwolk.spring.auth.dao.SqlDAO;
import ru.forwolk.spring.auth.dao.UserDataAccessObject;
import ru.forwolk.spring.auth.domain.User;

import static org.junit.Assert.*;

public class SqlDaoTest {
    private UserDataAccessObject userDAO;

    //Эталонные данные
    private static final String USER_FIRST_LOGIN = "Alexa342";
    private static final String ANOTHER_USER_LOGIN = "Morpex777";
    private static final String USER_PASSWORD = "dIf34f(Ut34gear";

    @Before
    public void sqlDaoInit() {
        if (userDAO == null) {
            userDAO = new SqlDAO();
        }
    }

    @Test
    public void userSqlDaoTest() {
        // Создаем объект пользователя
        User user = new User(USER_FIRST_LOGIN);
        user.setPassword(USER_PASSWORD);

        //Сохраняем пользователя в базу
        user = userDAO.create(user);

        //Получаем ID сохраненного пользователя в базе
        int id = user.getId();

        //Проверяем то, что у нас логин и пароль эталонные
        assertEquals(user.getLogin(), USER_FIRST_LOGIN);
        assertEquals(user.getPassword(), USER_PASSWORD);

        //Читаем пользователя из базы и проверяем, что он соответствует первоначальному пользователю
        User readUser = userDAO.read(id);
        assertEquals(readUser, user);

        //Меняем логин только что прочитанного пользователя и убеждаемся в том, что он не совпадает с начальным пользователем
        readUser.setLogin(ANOTHER_USER_LOGIN);
        assertNotEquals(readUser, user);

        //Обновляем пользователя в базе
        userDAO.update(readUser);

        //Читаем обнолвенного пользователя
        User letAnotherUser = userDAO.read(id);

        //Проверяем, что он не соответствует изначальному, но соответствует тому, которого мы только что обновили
        assertNotEquals(letAnotherUser, user);
        assertEquals(letAnotherUser, readUser);

        //Удаляем пользователя из базы
        userDAO.delete(id);

        //Пытаемся читать пользователя из базы и убеждаемся в том, что его более не существует
        User deletedUser = userDAO.read(id);
        assertNull(deletedUser);
    }
}
