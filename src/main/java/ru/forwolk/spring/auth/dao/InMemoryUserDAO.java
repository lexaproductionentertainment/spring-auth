package ru.forwolk.spring.auth.dao;


import org.springframework.stereotype.Component;
import ru.forwolk.spring.auth.domain.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InMemoryUserDAO implements UserDataAccessObject {

    private Map<Integer, User> map = new HashMap<>();
    private int counter = 1;

    public User create(User user) {
        if (user != null) {
            user.setId(counter);
            User u = user.clone();
            map.put(counter, u);
            counter++;
        }
        return user;

    }

    public User read(int id) {
        return map.get(id);
    }

    public void update(User user) {
        if (map.containsKey(user.getId())) {
            map.put(user.getId(), user);
        }
    }

    public int getSize() {
        return map.size();

    }

    public void delete(int id) {
        if (map.containsKey(id)) {
            map.remove(id);
        }
    }

    public List<User> getAll() {
        /** Здесь получаем из мапы список всех пользователей */
        return map.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public User getByLogin(String login) {
        User user = null;
        for(Map.Entry<Integer, User> entry : map.entrySet()){
            if (entry.getValue().getLogin().equals(login)){
                user = entry.getValue();
            }
        }
        return user;
    }
}