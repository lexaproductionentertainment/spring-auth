package ru.forwolk.spring.auth.dao;


import ru.forwolk.spring.auth.domain.User;

import java.util.List;

public interface UserDataAccessObject {
    /** Сохранение пользователя */
    User create(User user);
    /** Получение пользователя */
    User read(int id);
    /** Изменение существующего пользователя */
    void update(User user);
    /** Удаление пользователя */
    void delete(int id);
    /** Получить список всех пользователей */
    List<User> getAll();
    User getByLogin(String login);
}