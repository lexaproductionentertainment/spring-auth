package ru.forwolk.spring.auth.dao;

import ru.forwolk.spring.auth.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqlDAO implements UserDataAccessObject {

    private static final String CREATE_USER_QUERY_TEMPLATE = "INSERT INTO users (login, password) VALUES (?, ?)";
    private static final String READ_USER_QUERY_TEMPLATE = "SELECT * FROM users WHERE id = ?";
    private static final String UPDATE_USER_QUERY_TEMPLATE = "UPDATE users SET login = ?, password = ? WHERE id = ?";
    private static final String DELETE_USER_QUERY_TEMPLATE = "DELETE FROM users WHERE id = ?";
    private static final String GET_ALL_USER_QUERY_TEMPLATE = "SELECT * FROM users";
    private static final String GET_BY_LOGIN_USER_QUERY_TEMPLATE = "SELECT * FROM users WHERE login = ?";

    private Connection connection;

    public SqlDAO() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User create(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement(CREATE_USER_QUERY_TEMPLATE);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int id = resultSet.getInt(1);
            user.setId(id);
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public User read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(READ_USER_QUERY_TEMPLATE);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                resultSet.close();
                statement.close();
                User user = new User(login);
                user.setPassword(password);
                user.setId(id);
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_USER_QUERY_TEMPLATE);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getId());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(DELETE_USER_QUERY_TEMPLATE);
            statement.setInt(1, id);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAll(){
        try {
            PreparedStatement statement = connection.prepareStatement(GET_ALL_USER_QUERY_TEMPLATE);
            ResultSet resultSet = statement.executeQuery();
            List<User> list = new ArrayList();
            while (resultSet.next()){
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String email = resultSet.getString("email");
                int id = resultSet.getInt("id");
                User user = new User(login);
                user.setId(id);
                user.setPassword(password);
                user.setEmail(email);
                list.add(user);
                resultSet.next();
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getByLogin(String login){
        try {
            PreparedStatement statement = connection.prepareStatement(GET_BY_LOGIN_USER_QUERY_TEMPLATE);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String password = resultSet.getString("password");
                String email = resultSet.getString("email");
                int id = resultSet.getInt("id");
                User user = new User(login);
                user.setId(id);
                user.setEmail(email);
                user.setPassword(password);
                user.setLogin(login);
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
