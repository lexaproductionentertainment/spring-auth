package ru.forwolk.spring.auth.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.forwolk.spring.auth.dao.UserDataAccessObject;
import ru.forwolk.spring.auth.domain.User;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserDataAccessObject userDao;


    @PostMapping("create")
    public ResponseEntity<String> create(@RequestBody User user) {
        userDao.create(user);
        return ResponseEntity.ok(user.toString());
    }

    @GetMapping("getAll")
    public ResponseEntity<List<User>> getAllUser() {
        return ResponseEntity.ok(userDao.getAll());
    }

    @GetMapping("get")
    public ResponseEntity<User> get(@RequestParam int id){
        return ResponseEntity.ok(userDao.read(id));
    }

    @PutMapping("update")
    public ResponseEntity<String> update(@RequestBody User user) {
        userDao.update(user);
        return ResponseEntity.ok(user.toString());
    }

    @DeleteMapping("delete")
    public ResponseEntity<String> delete(@RequestParam int id){
        userDao.delete(id);
        return ResponseEntity.ok(userDao.toString());
    }

    @GetMapping("getByLogin")
    public ResponseEntity<User> getByLogin(@RequestParam String login) {
        if (userDao.getByLogin(login) == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(userDao.getByLogin(login));
        }
    }
}

