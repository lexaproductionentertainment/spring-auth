package ru.forwolk.spring.auth.domain;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@EqualsAndHashCode
public class User implements Cloneable{
    private int id;
    private String login;
    private String password;
    private String email;

    public User(String login) {
        this.login = login;
    }
    @Override
    public User clone() {
        User obj = null;
        try {
            obj = (User) super.clone();
        } catch(CloneNotSupportedException exc) {
            exc.printStackTrace();
        }
    return obj;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) { this.id = id; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj.getClass().equals(this.getClass())) {
            User anotherUser = (User) obj;

            boolean checkLogin, checkPassword, checkId;
             if (anotherUser.getLogin() == null && this.getLogin()== null) checkLogin = true;
             else checkLogin = anotherUser.getLogin().equals(this.getLogin());

             if (anotherUser.getPassword() == null && this.getPassword() == null) checkPassword = true;
             else checkPassword = anotherUser.getPassword().equals(this.getPassword());

             if (anotherUser.getId() == 0 && this.getId() == 0) checkId = true;
             else checkId = anotherUser.getId() ==(this.getId());

            return checkLogin && checkPassword && checkId;
        } else {
            return false;
        }
    }


    }

